import React from "react";
import ReactDOM from "react-dom/client";
import SchemaTable from "./component/SchemaTable";
import { IColumnConfig, IRenderData } from "./component";
import _ from "lodash";
import exampleData, { exampleDataSchema, IExampleData } from "./exampleData";
import "./component/index.scss";

export type { IColumnConfig, IRenderData };
export { SchemaTable };

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

const Test = () => {
  const [checkedIndexes, setCheckIndexes] = React.useState<number[]>([]);
  const onCheckedIndexHandler = React.useCallback(
    (rowIndexes: number[]) => {
      const diffIndexes = _.difference(rowIndexes, checkedIndexes);
      setCheckIndexes(
        diffIndexes.length !== 0
          ? [...checkedIndexes, ...diffIndexes]
          : checkedIndexes.filter((index) => !rowIndexes.includes(index))
      );
    },
    [checkedIndexes]
  );

  return (
    <div style={{ margin: 10 }}>
      <SchemaTable<IExampleData>
        isSortable
        isColumnFilterable
        isSearchable
        enableAutoFocus={false}
        checkedIndexes={checkedIndexes}
        onCheckedIndexesChange={onCheckedIndexHandler}
        schema={exampleDataSchema}
        data={(getDataProps) => {
          console.log(getDataProps);
          return new Promise((resolve, reject) => {
            setTimeout(() => resolve(exampleData), 3_000);
          });
        }}
        width={1500}
        maxHeight={window.innerHeight - 190}
        disabledCheckedIndexes={[0]}
        config={{
          jobId: {
            FilterForm: ({ propName, columnFilterValue, onChange }) => (
              <div className="input-group">
                <input
                  type="text"
                  className="form-control"
                  placeholder={`This is a custom input`}
                  aria-label={`Search ${propName}`}
                  autoFocus
                  value={`${columnFilterValue || ""}`}
                  data-prop-name={propName}
                  onChange={(e) => {
                    // @ts-ignore
                    onChange(e.currentTarget.value || undefined);
                  }}
                />
              </div>
            ),
          },
          insertDateTime: {
            width: 400,
          },

          taskType: {
            isFilterable: true,
            translation: {
              ttNoShaper: "No shaper",
              ttReportedProblem: "Reported problem",
              ttVisualCheck: "Visual check",
              ttNonArticle: "Non article",
            },
          },
        }}
        onRowClick={() => {
          console.log("row click");
        }}
        onRowDoubleClick={() => console.log("row double click")}
      />
    </div>
  );
};

root.render(<Test />);
