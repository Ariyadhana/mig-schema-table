import React from "react";
import { IFilterFormComponentProps } from "./SchemaTable/SchemaColumnFilterPopover/FilterFormComponent";

export interface IColumnConfig<T> {
  align?: "start" | "center" | "end";
  dateFormat?: string;
  defaultSortDesc?: boolean;
  FilterForm?: (props: IFilterFormComponentProps) => React.ReactElement | null;
  hidden?: boolean;
  hoverTitle?: string;
  isFilterable?: boolean;
  order?: number;
  renderCell?: (
    rowData: T,
    dataIndex: number,
    rowIndex: number
  ) => React.ReactElement | null;
  renderData?: (rowData: T, dataIndex: number) => string;
  showTimezones?: false;
  sort?: (a: T, b: T, sortAsc: boolean) => number;
  sortByValue?: boolean;
  sortable?: boolean;
  title?: string | React.ReactElement;
  translation?: { [keyName: string]: string };
  width?: number;
}

export interface IRenderData {
  _index: number;
  // @todo remove | any
  [key: string]: string | any;
}
