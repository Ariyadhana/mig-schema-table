import { upperFirst } from "lodash";

function isUpperCase(letter: string): boolean {
  return !!letter.match(/[A-Z]/);
}

export function uncamel(camel: string) {
  return upperFirst(
    camel.replaceAll(/[A-Z]/g, (letter, position) => {
      const prevLetter = camel[position - 1];
      const prevIsUppercase = prevLetter ? isUpperCase(prevLetter) : false;
      const nextLetter = camel[position + 1];
      const nextIsUppercase = nextLetter ? isUpperCase(nextLetter) : false;
      if (
        position === 0 ||
        (prevIsUppercase && (!nextLetter || nextIsUppercase))
      ) {
        return letter;
      }
      return ` ${nextIsUppercase ? letter : letter.toLowerCase()}`;
    })
  );
}

export function t(
  key: string,
  db: { [key: string]: string } = {
    "Europe/Amsterdam": "AMS",
    "Europe/Berlin": "AMS",
    "Asia/Jakarta": "JKT",
    "Asia/Bangkok": "JKT",
  }
) {
  return db[key] || uncamel(key);
}
