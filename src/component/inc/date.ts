import { format } from "date-fns";
import { formatInTimeZone } from "date-fns-tz";
import nl from "date-fns/locale/nl";

export const localeFormat = (date: Date | number, dateFormat: string): string =>
  format(date, dateFormat, { locale: nl });

export const localeFormatInTimeZone = (
  date: Date | number,
  timezone: string,
  dateFormat: string
): string => formatInTimeZone(date, timezone, dateFormat, { locale: nl });

// E.g. Europe/Amsterdam, Europe/Berlin, Asia/Jakarta, Asia/Bangkok...
export const timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
