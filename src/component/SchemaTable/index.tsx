import React, { Dispatch, SetStateAction } from "react";
import { oas31 } from "openapi3-ts";
import { VariableSizeGrid, VariableSizeList } from "react-window";
import { localeFormat } from "../inc/date";
import { t, uncamel } from "../inc/string";
import Th, { EColumnFilterStatus } from "./Th";
import { IColumnConfig, IRenderData } from "../types";
import { SELECT_ALL_COLUMN_NAME, SELECT_ALL_COLUMN_WIDTH } from "./constants";
import Td from "./Td";
import { DEFAULT_DATE_FORMAT, DEFAULT_DATE_TIME_FORMAT } from "../inc/constant";
import SchemaColumnFilterPopover, {
  ISchemaColumnFilterPopoverConfig,
} from "./SchemaColumnFilterPopover";

const startOfTheWorldDate = new Date("1000-01-01 00:00:00Z");

export interface IGetDataProps {
  searchQuery: string;
  columnFilterMap: {
    [propName: string]: TColumnFilterValue;
  };
  sortColumn?: string;
  sortAsc: boolean;
}

export interface ISchemaTableProps<T> {
  Heading?: any;
  checkedIndexes?: number[];
  disabledCheckedIndexes?: number[];
  config?: { [propName: string]: IColumnConfig<T> };
  customElement?: React.ReactNode;
  data: T[] | ((getDataProps: IGetDataProps) => Promise<T[]>);
  defaultSortAsc?: boolean;
  defaultSortColumn?: keyof T;
  getRowClassName?: (rowData: T, dataIndex: number) => string;
  getRowSelected?: (rowData: T, dataIndex: number) => boolean;
  maxHeight?: number;
  isSearchable?: boolean;
  enableAutoFocus?: boolean;
  isColumnFilterable?: boolean;
  isSortable?: boolean;
  onCheckedIndexesChange?: (dataIndex: number[]) => void;
  onRowClick?: (rowData: T, dataIndex: number, event: React.MouseEvent) => void;
  onRowDoubleClick?: (
    rowData: T,
    dataIndex: number,
    event: React.MouseEvent
  ) => void;
  rowHeight?: number;
  schema: oas31.SchemaObject;
  searchPlaceholder?: string;
  style?: React.CSSProperties;
  width: number;
}

export type TColumnFilterValue =
  | string
  | number
  | boolean
  | { from?: Date; to?: Date };

function SchemaTable<T>({
  Heading = VariableSizeList,
  checkedIndexes,
  disabledCheckedIndexes,
  config,
  customElement,
  data,
  defaultSortAsc = false,
  defaultSortColumn,
  getRowClassName,
  getRowSelected,
  maxHeight,
  isSearchable,
  isColumnFilterable,
  isSortable,
  onCheckedIndexesChange,
  onRowClick,
  rowHeight = 36,
  schema,
  searchPlaceholder,
  style,
  width,
  onRowDoubleClick,
  enableAutoFocus,
}: ISchemaTableProps<T>) {
  const [sortColumn, setSortColumn] = React.useState(
    defaultSortColumn as string
  );
  const [sortAsc, setSortAsc] = React.useState<boolean>(defaultSortAsc);
  const [searchQuery, setSearchQuery] = React.useState<string>("");
  const [columnFilterMap, setColumnFilterMap] = React.useState<{
    [propName: string]: TColumnFilterValue;
  }>({});
  const [popoverConfig, setPopoverConfig] =
    React.useState<ISchemaColumnFilterPopoverConfig>();
  const isDataFunction = data instanceof Function;
  const [sourceData, setSourceData] = React.useState<T[] | null | undefined>(
    isDataFunction ? undefined : data
  );

  const [isDirty, setIsDirty] = React.useState(false);

  React.useEffect(() => {
    if (isDataFunction) {
      return;
    }
    setSourceData(data);
  }, [data, isDataFunction]);

  React.useEffect(() => {
    if (!isDataFunction || sourceData !== undefined) {
      return;
    }
    data({
      searchQuery,
      columnFilterMap,
      sortColumn,
      sortAsc,
    }).then(setSourceData);
  }, [
    columnFilterMap,
    data,
    isDataFunction,
    searchQuery,
    sortAsc,
    sortColumn,
    sourceData,
  ]);

  const { properties = {} } = schema;
  const columnNames = React.useMemo<string[]>(() => {
    const columns = Object.keys(properties);
    if (onCheckedIndexesChange) {
      columns.unshift(SELECT_ALL_COLUMN_NAME);
    }
    if (!config) {
      return columns;
    }

    const invisibleColumns = Object.entries(config).reduce<string[]>(
      (prev, [propName, propConfig]) => {
        if (propConfig.hidden) {
          prev.push(propName);
        }
        return prev;
      },
      []
    );

    return columns
      .filter((key) => !invisibleColumns.includes(key))
      .sort((columnA, columnB) => {
        let orderA = config[columnA] ? config[columnA].order : undefined;
        if (orderA === undefined) {
          orderA = Object.keys(properties).findIndex(
            (propName) => propName === columnA
          );
        }
        let orderB = config[columnB] ? config[columnB].order : undefined;
        if (orderB === undefined) {
          orderB = Object.keys(properties).findIndex(
            (propName) => propName === columnB
          );
        }
        if (
          columnB === SELECT_ALL_COLUMN_NAME ||
          columnA === SELECT_ALL_COLUMN_NAME
        ) {
          return 0;
        }
        if (orderA === -1) {
          return 1;
        }
        if (orderB === -1) {
          return -1;
        }
        return orderA - orderB;
      });
  }, [config, onCheckedIndexesChange, properties]);

  const renderData = React.useMemo<IRenderData[] | undefined>(
    () =>
      sourceData
        ? sourceData.map((object, rowIndex) =>
            columnNames.reduce(
              (prev: IRenderData, propName) => {
                const schema = properties[propName] as oas31.SchemaObject;
                const propConfig = config ? config[propName] : undefined;
                if (propConfig?.renderData) {
                  prev[propName] = propConfig.renderData(object, rowIndex);
                  return prev;
                }

                if (!schema || propName === SELECT_ALL_COLUMN_NAME) {
                  prev[propName] = "?";
                  return prev;
                }
                const val = object[propName as keyof T] as any;
                const rawValue = !propConfig?.translation
                  ? val
                  : t(val, propConfig.translation);
                switch (schema.type) {
                  case "array":
                    prev[propName] =
                      (schema.items as oas31.SchemaObject)?.type === "string" &&
                      rawValue
                        ? rawValue.map(uncamel).join(", ")
                        : JSON.stringify(rawValue);
                    return prev;

                  case "boolean":
                    prev[propName] = rawValue ? "✓" : "✕";
                    return prev;

                  case "integer":
                    prev[propName] = [undefined, null].includes(rawValue)
                      ? ""
                      : `${rawValue}`;
                    return prev;

                  // @ts-ignore
                  case "string":
                    if (schema.format === "date" && rawValue) {
                      prev[propName] =
                        (rawValue as string) === "2999-12-31"
                          ? "-"
                          : localeFormat(
                              new Date(rawValue),
                              propConfig?.dateFormat || DEFAULT_DATE_FORMAT
                            );
                      return prev;
                    }
                    if (schema.format === "date-time" && rawValue) {
                      prev[propName] = localeFormat(
                        new Date(rawValue),
                        propConfig?.dateFormat || DEFAULT_DATE_TIME_FORMAT
                      );
                      return prev;
                    }
                    if (schema.enum) {
                      prev[propName] = rawValue ? uncamel(rawValue) : "";
                      return prev;
                    }
                  // fallthrough

                  default:
                    prev[propName] = rawValue ? `${rawValue}` : "";
                    return prev;
                }
              },
              { _index: rowIndex }
            )
          )
        : undefined,
    [columnNames, config, sourceData, properties]
  );

  const columnCount = columnNames.length;
  const { dynamicWidthColumnCount, fixedWidthColumnsWidth } =
    React.useMemo(() => {
      let fixedWidthColumnsWidth = 0;
      let dynamicWidthColumnCount = 0;
      columnNames.forEach((propName) => {
        if (propName === SELECT_ALL_COLUMN_NAME) {
          fixedWidthColumnsWidth += SELECT_ALL_COLUMN_WIDTH;
          return;
        }
        const propConfig = config ? config[propName] : undefined;
        if (propConfig?.width) {
          fixedWidthColumnsWidth += propConfig.width;
        } else {
          dynamicWidthColumnCount += 1;
        }
      }, 0);

      return { dynamicWidthColumnCount, fixedWidthColumnsWidth };
    }, [columnNames, config]);

  const columnWidths = React.useMemo(() => {
    const dynamicColumnWidth = Math.floor(
      (width - fixedWidthColumnsWidth) / dynamicWidthColumnCount
    );
    let roundingDifference =
      (width - fixedWidthColumnsWidth) % dynamicWidthColumnCount;
    return columnNames.map((propName) => {
      if (propName === SELECT_ALL_COLUMN_NAME) {
        return SELECT_ALL_COLUMN_WIDTH;
      }
      const propConfig = config ? config[propName] : undefined;
      if (propConfig?.width) {
        return propConfig?.width;
      }
      if (roundingDifference) {
        roundingDifference -= 1;
        return dynamicColumnWidth + 1;
      }
      return dynamicColumnWidth;
    });
  }, [
    columnNames,
    config,
    dynamicWidthColumnCount,
    fixedWidthColumnsWidth,
    width,
  ]);

  const getColumnWidth = React.useCallback(
    (columnIndex: number) => (columnWidths ? columnWidths[columnIndex] : 1),
    [columnWidths]
  );

  const filteredRenderData = React.useMemo(() => {
    if (
      !renderData ||
      (!isColumnFilterable && !isSearchable) ||
      isDataFunction
    ) {
      return renderData;
    }

    return renderData.filter((item) => {
      const lcSearchQuery = searchQuery.toLowerCase();
      if (
        searchQuery &&
        !columnNames.find((columnName) =>
          `${item[columnName]}`.toLowerCase().includes(lcSearchQuery)
        )
      ) {
        return false;
      }
      let result = true;
      Object.entries(columnFilterMap).forEach(
        ([propName, columnFilterValue]) => {
          if (!result || columnFilterValue === undefined) {
            return;
          }

          const propSchema = properties[propName] as oas31.SchemaObject;
          // @ts-ignore
          const rawValue = sourceData[item._index][propName];
          switch (propSchema.type) {
            case "boolean":
            case "integer":
              result = rawValue === columnFilterValue;
              break;

            // @ts-ignore
            case "string":
              if (
                typeof columnFilterValue === "object" &&
                (propSchema.format === "date" ||
                  propSchema.format === "date-time")
              ) {
                const { from, to } = columnFilterValue;
                const rawDate = rawValue ? new Date(rawValue) : undefined;
                if (
                  (from && (!rawDate || rawDate < from)) ||
                  (to && (!rawDate || rawDate > to))
                ) {
                  result = false;
                }
                return;
              }
            // fall through

            default:
              // fallback by looking at the render value
              result = `${item[propName]}`
                .toLowerCase()
                .includes(`${columnFilterValue}`.toLowerCase());
          }
        }
      );
      return result;
    });
  }, [
    renderData,
    isColumnFilterable,
    isSearchable,
    isDataFunction,
    searchQuery,
    columnNames,
    columnFilterMap,
    properties,
    sourceData,
  ]);

  // Sort the filtered data
  const sortedRenderData = React.useMemo(() => {
    if (!sortColumn || !filteredRenderData || !sourceData || isDataFunction) {
      return filteredRenderData;
    }
    const sortSchema = properties[sortColumn as string] as
      | oas31.SchemaObject
      | undefined;

    const propConfig = config ? config[sortColumn] : undefined;
    const columnSort = propConfig?.sort;
    if (columnSort) {
      return filteredRenderData.sort((a, b) => {
        const aData = sourceData[a._index];
        const bData = sourceData[b._index];
        if (!aData) {
          return 1;
        }
        if (!bData) {
          return -1;
        }
        return columnSort(aData, bData, sortAsc);
      });
    }

    return filteredRenderData.sort((a, b) => {
      const isDate = sortSchema && sortSchema.format?.startsWith("date");
      const sortByValue =
        propConfig?.sortByValue === undefined
          ? !sortSchema ||
            isDate ||
            sortSchema.type === "boolean" ||
            sortSchema.type === "integer" ||
            propConfig?.renderCell
          : propConfig.sortByValue;
      let x =
        sortByValue && sourceData[a._index]
          ? // @ts-ignore
            sourceData[a._index][sortColumn]
          : a[sortColumn as string].toLowerCase();
      let y =
        sortByValue && sourceData[b._index]
          ? // @ts-ignore
            sourceData[b._index][sortColumn]
          : b[sortColumn as string].toLowerCase();
      if (sortByValue && isDate) {
        x = new Date(x);
        if (isNaN(x)) {
          x = startOfTheWorldDate;
        }
        y = new Date(y);
        if (isNaN(y)) {
          y = startOfTheWorldDate;
        }
      }

      if (x === y) {
        return 0;
      }
      return (x < y ? 1 : -1) * (sortAsc ? -1 : 1);
    });
  }, [
    sortColumn,
    filteredRenderData,
    sourceData,
    isDataFunction,
    properties,
    config,
    sortAsc,
  ]);

  const onSelectAllIndexesHandler = React.useCallback(() => {
    if (!onCheckedIndexesChange || !filteredRenderData) {
      return;
    }
    onCheckedIndexesChange(
      filteredRenderData
        .map((el) => el._index)
        .filter((index) => !disabledCheckedIndexes?.includes(index))
    );
  }, [onCheckedIndexesChange, filteredRenderData, disabledCheckedIndexes]);

  const isAllRowsChecked = React.useMemo(() => {
    const tableData = [...(filteredRenderData || [])].filter(
      (el) =>
        (checkedIndexes ? checkedIndexes.includes(el._index) : true) ||
        (disabledCheckedIndexes
          ? !disabledCheckedIndexes.includes(el._index)
          : true)
    );
    return (
      checkedIndexes?.length !== 0 &&
      tableData.length === checkedIndexes?.length
    );
  }, [checkedIndexes, disabledCheckedIndexes, filteredRenderData]);

  const disableColumnFilter = React.useCallback(
    (propName: string) => {
      const newColumnFilterMap = { ...columnFilterMap };
      delete newColumnFilterMap[propName];
      if (isDataFunction) {
        setIsDirty(true);
      }
      setColumnFilterMap(newColumnFilterMap);
    },
    [columnFilterMap, isDataFunction]
  );

  const onSetSortColumn = React.useCallback<Dispatch<SetStateAction<string>>>(
    (x) => {
      if (isDataFunction) {
        setIsDirty(true);
      }
      setSortColumn(x);
    },
    [isDataFunction]
  );

  const onSetSortAsc = React.useCallback<Dispatch<SetStateAction<boolean>>>(
    (x) => {
      if (isDataFunction) {
        setIsDirty(true);
      }
      setSortAsc(x);
    },
    [isDataFunction]
  );

  const SchemaTableTh = React.useCallback(
    ({ style, index }: { style: React.CSSProperties; index: number }) => {
      const propName = columnNames[index];
      const propConfig = config ? config[propName] : undefined;
      let columnFilterStatus =
        isColumnFilterable && propConfig?.isFilterable !== false
          ? EColumnFilterStatus.AVAILABLE
          : EColumnFilterStatus.UNAVAILABLE;
      if (columnFilterMap[propName] !== undefined) {
        columnFilterStatus = EColumnFilterStatus.ACTIVE;
      }
      const schema = (
        propName === SELECT_ALL_COLUMN_NAME
          ? { type: "boolean" }
          : properties[propName]
      ) as oas31.SchemaObject;

      return (
        <Th
          isAllChecked={isAllRowsChecked}
          columnFilterStatus={columnFilterStatus}
          disableColumnFilter={disableColumnFilter}
          isSortable={!!isSortable}
          numberOfSelectedRows={checkedIndexes?.length}
          onSelectAllIndexesHandler={onSelectAllIndexesHandler}
          propConfig={propConfig}
          propName={propName}
          schema={schema}
          setPopoverConfig={setPopoverConfig}
          setSortAsc={onSetSortAsc}
          setSortColumn={onSetSortColumn}
          sortAsc={sortColumn === propName ? sortAsc : undefined}
          style={style}
        />
      );
    },
    [
      checkedIndexes?.length,
      columnFilterMap,
      columnNames,
      config,
      disableColumnFilter,
      isAllRowsChecked,
      isColumnFilterable,
      isSortable,
      onSelectAllIndexesHandler,
      onSetSortAsc,
      onSetSortColumn,
      properties,
      sortAsc,
      sortColumn,
    ]
  );

  const SchemaTableTd = React.useCallback(
    ({ columnIndex, rowIndex, style }: any) => {
      const propName = columnNames[columnIndex];
      const propSchema = (
        propName === SELECT_ALL_COLUMN_NAME
          ? { type: "boolean" }
          : properties[propName]
      ) as oas31.SchemaObject;
      return sourceData ? (
        <Td
          checkedIndexes={checkedIndexes}
          disabledCheckedIndexes={disabledCheckedIndexes}
          columnIndex={columnIndex}
          data={sourceData}
          getRowClassName={getRowClassName}
          getRowSelected={getRowSelected}
          onCheckedIndexesChange={onCheckedIndexesChange}
          onRowClick={onRowClick}
          onRowDoubleClick={onRowDoubleClick}
          propConfig={config ? config[propName] : undefined}
          propName={propName}
          propSchema={propSchema}
          rowHeight={rowHeight}
          rowIndex={rowIndex}
          sortedRenderData={sortedRenderData}
          style={style}
        />
      ) : null;
    },
    [
      columnNames,
      properties,
      sourceData,
      checkedIndexes,
      disabledCheckedIndexes,
      getRowClassName,
      getRowSelected,
      onCheckedIndexesChange,
      onRowClick,
      onRowDoubleClick,
      config,
      rowHeight,
      sortedRenderData,
    ]
  );

  const onSearchChange = React.useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      if (isDataFunction) {
        setIsDirty(true);
      }
      setSearchQuery(e.currentTarget.value);
    },
    [isDataFunction]
  );

  const refreshData = React.useCallback(() => {
    setIsDirty(false);
    setSourceData(undefined);
  }, []);

  const onInputKeyDown = React.useCallback(
    (e: React.KeyboardEvent) => {
      if (e.key === "Enter") {
        setPopoverConfig(undefined);
        if (isDirty) {
          refreshData();
        }
      }
    },
    [isDirty, refreshData]
  );

  const getRowHeight = React.useCallback(() => rowHeight, [rowHeight]);
  const rowWidth = dynamicWidthColumnCount ? width : fixedWidthColumnsWidth;

  const rowCount = React.useMemo(
    () => (sortedRenderData ? sortedRenderData.length : 0),
    [sortedRenderData]
  );

  const tableBodyHeight = React.useMemo(() => {
    const rowsHeight = rowHeight * rowCount;
    const rowsMaxHeight = maxHeight ? maxHeight - (isSearchable ? 50 : 0) : 0;
    return rowsMaxHeight && rowsMaxHeight < rowsHeight
      ? rowsMaxHeight
      : rowsHeight;
  }, [maxHeight, isSearchable, rowCount, rowHeight]);

  const onPopoverClose = React.useCallback(() => {
    setPopoverConfig(undefined);
  }, []);

  const onSchemaColumnFilterChange = React.useCallback(
    (newColumnFilterValue?: TColumnFilterValue) => {
      if (!popoverConfig) {
        return;
      }

      if (isDataFunction) {
        setIsDirty(true);
      }

      if (newColumnFilterValue === undefined) {
        disableColumnFilter(popoverConfig.propName);
        return;
      }

      setColumnFilterMap((columnFilterMap) => ({
        ...columnFilterMap,
        [popoverConfig.propName]: newColumnFilterValue,
      }));
    },
    [disableColumnFilter, isDataFunction, popoverConfig]
  );

  const searchInputAutoFocus = React.useMemo(() => {
    if (enableAutoFocus === undefined) {
      return true;
    }
    return enableAutoFocus;
  }, [enableAutoFocus]);

  return (
    <div
      className={`schema-table${
        onRowClick ? " schema-table--clickable-rows" : ""
      }`}
      style={{
        ...style,
        width: rowWidth,
      }}
      role="table"
    >
      <div className={"schema-table__action-container"}>
        <div style={{ flex: 1 }}>
          {isSearchable ? (
            <input
              type="text"
              placeholder={searchPlaceholder || "Search..."}
              value={searchQuery}
              onChange={onSearchChange}
              onKeyDown={onInputKeyDown}
              autoFocus={searchInputAutoFocus}
            />
          ) : null}
        </div>
        {customElement}
      </div>
      <Heading
        key={`thead_${rowWidth}_${sortColumn}_${sortAsc}_${searchQuery}`}
        height={50}
        itemCount={columnCount}
        itemSize={getColumnWidth}
        layout="horizontal"
        width={rowWidth}
        sortAsc={sortAsc}
        setSortAsc={onSetSortAsc}
        setSortColumn={onSetSortColumn}
        sortColumn={sortColumn}
        sortedRenderData={sortedRenderData}
        className="schema-table__th-row"
      >
        {SchemaTableTh}
      </Heading>
      {sourceData && !isDirty ? (
        <VariableSizeGrid
          className="schema-table__tbody"
          key={`tbody_${tableBodyHeight}_${rowWidth}_${sortColumn}_${sortAsc}_${searchQuery}_${columnCount}`}
          height={tableBodyHeight}
          width={rowWidth}
          columnWidth={getColumnWidth}
          rowHeight={getRowHeight}
          columnCount={columnCount}
          rowCount={rowCount}
        >
          {SchemaTableTd}
        </VariableSizeGrid>
      ) : (
        <div
          style={{
            width: rowWidth,
            height: Math.max(50, tableBodyHeight),
            border: "1px solid #BBB",
            textAlign: "center",
            display: "flex",
            backgroundColor: "#CCC",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          {isDirty ? (
            <button onClick={refreshData} className="btn border">
              Refresh data
            </button>
          ) : (
            <div>⌛ Loading...</div>
          )}
        </div>
      )}
      {popoverConfig ? (
        <SchemaColumnFilterPopover
          referenceElement={popoverConfig.referenceElement}
          onClose={onPopoverClose}
          onChange={onSchemaColumnFilterChange}
          onInputKeyDown={onInputKeyDown}
          propName={popoverConfig.propName}
          propSchema={
            schema.properties![popoverConfig.propName] as oas31.SchemaObject
          }
          value={columnFilterMap[popoverConfig.propName]}
          propConfig={popoverConfig.propConfig}
        />
      ) : null}
    </div>
  );
}

export default React.memo(SchemaTable) as typeof SchemaTable;
