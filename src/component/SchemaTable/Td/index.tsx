import React from "react";
import { oas31 } from "openapi3-ts";
import { SELECT_ALL_COLUMN_NAME } from "../constants";
import { IColumnConfig, IRenderData } from "../../types";
import { localeFormatInTimeZone, timeZone } from "../../inc/date";
import { DEFAULT_DATE_TIME_FORMAT } from "../../inc/constant";
import { t } from "../../inc/string";

interface ITdProps<T> {
  checkedIndexes?: number[];
  disabledCheckedIndexes?: number[];
  columnIndex: number;
  data: T[];
  getRowClassName?: (rowData: T, dataIndex: number) => string;
  getRowSelected?: (rowData: T, dataIndex: number) => boolean;
  rowHeight: number;
  rowIndex: number;
  onCheckedIndexesChange?: (dataIndex: number[]) => void;
  onRowClick?: (rowData: T, dataIndex: number, event: React.MouseEvent) => void;
  onRowDoubleClick?: (
    rowData: T,
    dataIndex: number,
    event: React.MouseEvent
  ) => void;
  propConfig?: IColumnConfig<any>;
  propName: string;
  propSchema: oas31.SchemaObject;
  sortedRenderData?: IRenderData[];
  style: React.CSSProperties;
}

function Td<T>({
  checkedIndexes,
  disabledCheckedIndexes,
  columnIndex,
  data,
  getRowClassName,
  getRowSelected,
  onCheckedIndexesChange,
  onRowClick,
  onRowDoubleClick,
  propConfig,
  propName,
  propSchema,
  rowHeight,
  rowIndex,
  sortedRenderData,
  style,
}: ITdProps<T>) {
  const onTdClick = React.useCallback(
    (e: React.MouseEvent<HTMLDivElement>) => {
      if (!sortedRenderData || !onRowClick) {
        return;
      }
      const { rowIndex } = e.currentTarget.dataset;
      if (!rowIndex) {
        return;
      }
      const row = sortedRenderData[parseInt(rowIndex, 10)];
      onRowClick(data[row._index], row._index, e);
    },
    [data, onRowClick, sortedRenderData]
  );

  const onTdDoubleClick = React.useCallback(
    (e: React.MouseEvent<HTMLDivElement>) => {
      if (!sortedRenderData || !onRowDoubleClick) {
        return;
      }
      const { rowIndex } = e.currentTarget.dataset;
      if (!rowIndex) {
        return;
      }
      const row = sortedRenderData[parseInt(rowIndex, 10)];
      onRowDoubleClick(data[row._index], row._index, e);
    },
    [data, onRowDoubleClick, sortedRenderData]
  );

  const row = sortedRenderData ? sortedRenderData[rowIndex] : undefined;
  const tdDivProps = React.useMemo(() => {
    if (!row) {
      return undefined;
    }

    let title = propName === SELECT_ALL_COLUMN_NAME ? null : row[propName];
    if (
      propSchema.format &&
      propSchema.format.startsWith("date") &&
      propConfig?.showTimezones !== false
    ) {
      // @ts-ignore
      const dateString: string | undefined = data[row._index][propName];
      const date = dateString ? new Date(dateString) : undefined;
      const alternativeTimeZone = timeZone.startsWith("Europe/")
        ? "Asia/Jakarta"
        : "Europe/Amsterdam";
      if (date) {
        title = `${localeFormatInTimeZone(
          date,
          alternativeTimeZone,
          DEFAULT_DATE_TIME_FORMAT
        )} (${t(alternativeTimeZone)})`;
      }
    }

    return {
      "data-row-index": rowIndex,
      "data-column-index": columnIndex,
      key: propName,
      style: {
        ...style,
        lineHeight: `${rowHeight}px`,
      },
      onClick: !propConfig?.renderCell ? onTdClick : undefined,
      onDoubleClick: onTdDoubleClick,
      className: `schema-table__td schema-table__td--${
        rowIndex % 2 ? "odd" : "even"
      }${
        getRowSelected && getRowSelected(data[row._index], row._index)
          ? " schema-table__td--selected"
          : ""
      } ${
        getRowClassName ? getRowClassName(data[row._index], row._index) : ""
      }`,
      title,
    };
  }, [
    columnIndex,
    data,
    getRowClassName,
    getRowSelected,
    onTdClick,
    onTdDoubleClick,
    propConfig?.renderCell,
    propConfig?.showTimezones,
    propName,
    propSchema.format,
    row,
    rowHeight,
    rowIndex,
    style,
  ]);

  if (!row || !tdDivProps) {
    return null;
  }

  if (propConfig?.renderCell) {
    return (
      <div {...tdDivProps}>
        {propConfig.renderCell(data[row._index], row._index, rowIndex)}
      </div>
    );
  }

  if (propName === SELECT_ALL_COLUMN_NAME) {
    const onChecked = () => {
      if (!onCheckedIndexesChange) {
        return;
      }
      onCheckedIndexesChange([row._index]);
    };
    return (
      <div {...tdDivProps}>
        <div style={{ textAlign: "center" }}>
          <input
            data-key={"row-checkbox"}
            type={"checkbox"}
            onChange={onChecked}
            checked={checkedIndexes?.includes(row._index)}
            disabled={disabledCheckedIndexes?.includes(row._index)}
          />
        </div>
      </div>
    );
  }

  if (!propSchema) {
    return null;
  }

  const propValue = row[propName];

  switch (propSchema.type) {
    case "boolean":
      tdDivProps.className += ` text-${propConfig?.align || "center"}`;
      break;
    case "number":
    case "integer":
      tdDivProps.className += ` text-${propConfig?.align || "end"}`;
      break;
    default:
      if (propConfig?.align) {
        tdDivProps.className += ` text-${propConfig.align}`;
      }
      if (propSchema.format === "url" && propValue) {
        return (
          // @ts-ignore
          <a
            href={propValue}
            target="_blank"
            rel="noopener noreferrer"
            {...tdDivProps}
          >
            {propValue}
          </a>
        );
      }
  }
  return <div {...tdDivProps}>{propValue}</div>;
}

export default React.memo(Td) as typeof Td;
