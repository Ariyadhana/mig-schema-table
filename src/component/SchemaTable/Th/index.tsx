import { oas31 } from "openapi3-ts";
import React, { CSSProperties, Dispatch, SetStateAction } from "react";
import { IColumnConfig } from "../../types";
import { t, uncamel } from "../../inc/string";
import { SELECT_ALL_COLUMN_NAME } from "../constants";
import { ISchemaColumnFilterPopoverConfig } from "../SchemaColumnFilterPopover";
import { timeZone } from "../../inc/date";

export enum EColumnFilterStatus {
  UNAVAILABLE = "UNAVAILABLE",
  AVAILABLE = "AVAILABLE",
  ACTIVE = "ACTIVE",
}

interface IThProps {
  columnFilterStatus: EColumnFilterStatus;
  disableColumnFilter: (propName: string) => void;
  isAllChecked?: boolean;
  isSortable: boolean;
  numberOfSelectedRows?: number;
  onSelectAllIndexesHandler?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  propConfig?: IColumnConfig<any>;
  propName: string;
  schema: oas31.SchemaObject;
  setPopoverConfig?: Dispatch<
    SetStateAction<ISchemaColumnFilterPopoverConfig | undefined>
  >;
  setSortAsc: Dispatch<SetStateAction<boolean>>;
  setSortColumn: Dispatch<SetStateAction<string>>;
  sortAsc?: boolean;
  style: CSSProperties;
}

const Th = ({
  isAllChecked,
  columnFilterStatus,
  disableColumnFilter,
  isSortable,
  numberOfSelectedRows,
  onSelectAllIndexesHandler,
  propConfig,
  propName,
  schema,
  setPopoverConfig,
  setSortAsc,
  setSortColumn,
  sortAsc,
  style,
}: IThProps) => {
  const thDivProps = {
    style,
    className: `schema-table__th schema-table__th--column-filter-status-${columnFilterStatus} ${
      isSortable ? "schema-table__th--sortable" : "schema-table__th--unsortable"
    }`,
  };

  const onSortButtonClick = React.useCallback(() => {
    if (sortAsc === undefined) {
      setSortColumn(propName);
      setSortAsc(!propConfig?.defaultSortDesc);
      return;
    }
    setSortAsc((sortAsc) => !sortAsc);
  }, [
    propConfig?.defaultSortDesc,
    propName,
    setSortAsc,
    setSortColumn,
    sortAsc,
  ]);

  const onFilterButtonClick = React.useCallback(
    (e: React.MouseEvent<HTMLElement>) => {
      if (
        columnFilterStatus === EColumnFilterStatus.ACTIVE &&
        disableColumnFilter
      ) {
        disableColumnFilter(propName);
        return;
      }

      if (!setPopoverConfig) {
        return;
      }
      const referenceElement = e.currentTarget;
      setPopoverConfig((popoverConfig) => {
        if (popoverConfig) {
          return undefined;
        }

        return {
          referenceElement,
          propName,
          propConfig,
        };
      });
    },
    [
      columnFilterStatus,
      disableColumnFilter,
      propConfig,
      propName,
      setPopoverConfig,
    ]
  );

  const labelBody = React.useMemo(() => {
    if (propConfig?.title !== undefined) {
      return propConfig.title;
    }

    if (
      schema.format &&
      schema.format.startsWith("date") &&
      propConfig?.showTimezones !== false
    ) {
      return `${uncamel(propName)} (${t(timeZone)})`;
    }

    return uncamel(propName);
  }, [propConfig, propName, schema.format]);

  if (propName === SELECT_ALL_COLUMN_NAME) {
    return (
      <div {...thDivProps}>
        <div
          style={{
            width: "100%",
            textAlign: "center",
          }}
          title={`${numberOfSelectedRows || 0} selected`}
        >
          <input
            type={"checkbox"}
            checked={isAllChecked}
            onChange={onSelectAllIndexesHandler}
          />
        </div>
      </div>
    );
  }

  if (!schema) {
    return <div {...thDivProps} />;
  }
  switch (schema.type) {
    case "boolean":
      thDivProps.className += ` text-${
        propConfig?.align || "center"
      } justify-content-${propConfig?.align || "center"}`;
      break;
    case "integer":
    case "number":
      thDivProps.className += ` text-${
        propConfig?.align || "end"
      } justify-content-${propConfig?.align || "end"}`;
      break;
    default:
      if (propConfig?.align) {
        thDivProps.className += ` text-${propConfig.align}`;
      }
  }

  return (
    <div {...thDivProps}>
      {isSortable ? (
        <button
          className="px-0"
          disabled={propConfig?.sortable === false}
          onClick={onSortButtonClick}
        >
          {labelBody}
          {sortAsc === undefined ? null : sortAsc ? "▲" : "▼"}
        </button>
      ) : (
        <div style={{ lineHeight: "44px" }}>{labelBody}</div>
      )}
      {columnFilterStatus !== EColumnFilterStatus.UNAVAILABLE ? (
        <button onClick={onFilterButtonClick} data-bs-toggle="dropdown">
          <svg
            viewBox="0 0 36 36"
            xmlns="http://www.w3.org/2000/svg"
            height={16}
            width={16}
            style={{ display: "block" }}
            fill="currentColor"
          >
            <polygon points="14,30 22,25 22,17 35.999,0 17.988,0 0,0 14,17 " />
          </svg>
        </button>
      ) : null}
    </div>
  );
};

export default React.memo(Th) as typeof Th;
