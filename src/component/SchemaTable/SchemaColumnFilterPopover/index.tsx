import React from "react";
import { oas31 } from "openapi3-ts";
import { TColumnFilterValue } from "../index";
import FilterFormComponent from "./FilterFormComponent";
import { usePopper } from "react-popper";
import { IColumnConfig } from "../../types";

export interface ISchemaColumnFilterPopoverConfig {
  referenceElement: HTMLElement;
  propName: string;
  propConfig?: IColumnConfig<any>;
}

type TSchemaColumnFilterPopoverProps = ISchemaColumnFilterPopoverConfig & {
  onChange: (newValue?: TColumnFilterValue) => void;
  onInputKeyDown: (e: React.KeyboardEvent) => void;
  propSchema: oas31.SchemaObject;
  value: TColumnFilterValue;
  onClose: () => void;
};

const SchemaColumnFilterPopover = ({
  onChange,
  onClose,
  onInputKeyDown,
  propName,
  propSchema,
  referenceElement,
  value,
  propConfig,
}: TSchemaColumnFilterPopoverProps) => {
  const [popperElement, setPopperElement] =
    React.useState<HTMLDivElement | null>(null);
  const [arrowElement, setArrowElement] = React.useState<HTMLDivElement | null>(
    null
  );
  const { styles, attributes } = usePopper(referenceElement, popperElement, {
    modifiers: [{ name: "arrow", options: { element: arrowElement } }],
  });

  React.useEffect(() => {
    const onWindowClick = (e: MouseEvent) => {
      if (!popperElement) {
        return;
      }

      let parent = e.target as null | ParentNode;
      while (parent && popperElement) {
        if (parent === popperElement) {
          return;
        }
        parent =
          parent.parentNode === window.document ? null : parent.parentNode;
      }
      onClose();
    };
    window.addEventListener("click", onWindowClick, { capture: true });
    return () => {
      window.removeEventListener("click", onWindowClick, { capture: true });
    };
  }, [onClose, popperElement]);

  const classNames = ["popover", "schema-column-filter-popover"];
  if (attributes.popper) {
    classNames.push(`bs-popover-${attributes.popper["data-popper-placement"]}`);
  }

  const FilterForm = propConfig?.FilterForm || FilterFormComponent;

  return (
    <div
      className={classNames.join(" ")}
      role="tooltip"
      ref={setPopperElement}
      style={styles.popper}
      {...attributes.popper}
    >
      <div
        className="popover-arrow"
        ref={setArrowElement}
        style={styles.arrow}
      />
      <div className="popover-body">
        <FilterForm
          onChange={onChange}
          onInputKeyDown={onInputKeyDown}
          propSchema={propSchema}
          propName={propName}
          columnFilterValue={value}
          propConfig={propConfig}
        />
      </div>
    </div>
  );
};

export default React.memo(SchemaColumnFilterPopover);
