import React from "react";
import { oas31 } from "openapi3-ts";
import { t } from "../../../inc/string";
import {
  DEFAULT_DATE_FORMAT,
  DEFAULT_DATE_TIME_FORMAT,
} from "../../../inc/constant";
import DatePicker from "react-datepicker";
import nl from "date-fns/locale/nl";
import { TColumnFilterValue } from "../../index";
import { IColumnConfig } from "../../../types";

export interface IFilterFormComponentProps {
  columnFilterValue: TColumnFilterValue;
  onChange: (newValue?: TColumnFilterValue) => void;
  onInputKeyDown: (e: React.KeyboardEvent) => void;
  propConfig?: IColumnConfig<any>;
  propName: string;
  propSchema: oas31.SchemaObject;
}
const FilterFormComponent = ({
  columnFilterValue,
  onChange,
  onInputKeyDown,
  propConfig,
  propName,
  propSchema,
}: IFilterFormComponentProps) => {
  const { type, format, minimum, maximum } = propSchema;
  const value = columnFilterValue;

  switch (type) {
    case "integer":
      return (
        <div className="input-group">
          <input
            autoFocus
            className="form-control"
            type={"number"}
            value={(value || "") as string}
            data-prop-name={propName}
            onChange={(e) => {
              onChange(
                e.currentTarget.value === ""
                  ? undefined
                  : parseInt(e.currentTarget.value)
              );
            }}
            onKeyDown={onInputKeyDown}
            placeholder={`Search ${propName}`}
            min={minimum}
            max={maximum}
          />
        </div>
      );
    case "boolean":
      let selectValue = value ? "✓" : "✕";
      if (value === undefined) {
        selectValue = "";
      }
      return (
        <select
          autoFocus
          className="form-select"
          value={selectValue}
          data-prop-name={propName}
          onChange={(e) => {
            switch (e.currentTarget.value) {
              case "✓":
                onChange(true);
                break;

              case "✕":
                onChange(false);
                break;

              default:
                onChange(undefined);
            }
          }}
        >
          <option key={"all"} value={""}>
            All
          </option>
          {["✓", "✕"].map((optionValue: string) => (
            <option
              key={`column-filter-select-${optionValue}`}
              value={optionValue}
            >
              {optionValue}
            </option>
          ))}
        </select>
      );

    // @ts-ignore
    case "string":
      if (propSchema.enum) {
        return (
          <select
            autoFocus
            className="form-select"
            value={value as string}
            data-prop-name={propName}
            onChange={(e) => {
              onChange(e.currentTarget.value || undefined);
            }}
          >
            <option key={"all"} value={""}>
              All
            </option>
            {propSchema.enum.map((name: string) => {
              const rowName = !propConfig?.translation
                ? name
                : t(name, propConfig.translation);
              return (
                <option key={`column-filter-select-${rowName}`} value={rowName}>
                  {rowName}
                </option>
              );
            })}
          </select>
        );
      }
      if (format === "date-time" || format === "date") {
        const dateFormat =
          format === "date" ? DEFAULT_DATE_FORMAT : DEFAULT_DATE_TIME_FORMAT;
        const dateRangeValue = (columnFilterValue || {
          from: undefined,
          to: undefined,
        }) as {
          from?: Date;
          to?: Date;
        };
        const startDateChangeHandler = (date: Date | null) => {
          if (!date && !dateRangeValue.to) {
            onChange(undefined);
            return;
          }

          if (dateRangeValue.to && date && date > dateRangeValue.to) {
            return;
          }
          onChange({
            ...(columnFilterValue as object),
            from: date || undefined,
          });
        };
        const endDateChangeHandler = (date: Date | null) => {
          if (!date && !dateRangeValue.from) {
            onChange(undefined);
            return;
          }

          if (dateRangeValue.from && date && date < dateRangeValue.from) {
            return;
          }
          onChange({
            ...(columnFilterValue as object),
            to: date || undefined,
          });
        };
        return (
          <div className={"input-group d-flex"}>
            <div className={"d-flex flex-column m-1"}>
              <label>Start date-time</label>
              <DatePicker
                autoFocus
                dateFormat={dateFormat}
                data-prop-name={propName}
                locale={nl}
                selected={dateRangeValue.from}
                onChange={startDateChangeHandler}
                placeholderText={dateFormat}
                isClearable
                selectsStart
                showTimeSelect={format === "date-time"}
                timeIntervals={15}
                shouldCloseOnSelect={format === "date"}
              />
            </div>
            <div className={"d-flex flex-column m-1"}>
              <label>End date-time</label>
              <DatePicker
                id={"filter-date"}
                dateFormat={dateFormat}
                data-prop-name={propName}
                locale={nl}
                selectsEnd
                selected={dateRangeValue.to}
                onChange={endDateChangeHandler}
                placeholderText={dateFormat}
                isClearable
                startDate={dateRangeValue.from}
                endDate={dateRangeValue.to}
                showTimeSelect={format === "date-time"}
                timeIntervals={15}
                shouldCloseOnSelect={format === "date"}
              />
            </div>
          </div>
        );
      }
    // falls through

    default:
      return (
        <div className="input-group">
          <input
            autoFocus
            type="text"
            className="form-control"
            placeholder={`Search ${propName}`}
            aria-label={`Search ${propName}`}
            value={(value || "") as string}
            data-prop-name={propName}
            onChange={(e) => {
              onChange(e.currentTarget.value || undefined);
            }}
            onKeyDown={onInputKeyDown}
          />
        </div>
      );
  }
};

export default React.memo(FilterFormComponent);
