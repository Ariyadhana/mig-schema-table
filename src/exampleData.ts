import { oas31 } from "openapi3-ts";

export interface IExampleData {
  jobId: string;
  /** @enum {string} */
  jobType: "scrape" | "clean";
  topLevelId: number;
  domainId: string;
  hostId: string;
  sourceId: string;
  /** @enum {string} */
  sources: ("fromIndexPage" | "manual")[];
  priority: number;
  /** Format: date-time */
  insertDateTime: string;
  /** Format: date-time */
  scrapeStartDateTime?: string;
  /** Format: date-time */
  scrapeEndDateTime?: string;
  scraperHost?: string;
  scraperContainer?: string;
  errorMessage?: string;
  /** Format: url */
  url?: string;
  retryCount: number;
  isError: boolean;
  taskType:
    | "ttNoShaper"
    | "ttReportedProblem"
    | "ttVisualCheck"
    | "ttNonArticle";
}

export const exampleDataSchema: oas31.SchemaObject = {
  type: "object",
  properties: {
    jobId: {
      type: "string",
    },
    jobType: {
      type: "string",
      enum: ["scrape", "clean"],
    },
    topLevelId: {
      type: "integer",
    },
    domainId: {
      type: "string",
    },
    hostId: {
      type: "string",
    },
    sourceId: {
      type: "string",
    },
    sources: {
      type: "array",
      items: {
        type: "string",
        enum: ["fromIndexPage", "manual"],
      },
    },
    priority: {
      type: "integer",
      minimum: 1,
      maximum: 9,
    },
    insertDateTime: {
      type: "string",
      format: "date-time",
    },
    scrapeStartDateTime: {
      type: "string",
      format: "date-time",
    },
    scrapeEndDateTime: {
      type: "string",
      format: "date-time",
    },
    scraperHost: {
      type: "string",
    },
    scraperContainer: {
      type: "string",
    },
    errorMessage: {
      type: "string",
    },
    url: {
      type: "string",
      format: "url",
    },
    retryCount: {
      type: "integer",
    },
    isError: {
      type: "boolean",
    },
    taskType: {
      type: "string",
      enum: [
        "ttNoShaper",
        "ttReportedProblem",
        "ttVisualCheck",
        "ttNonArticle",
      ],
    },
  },
  required: [
    "jobId",
    "jobType",
    "topLevelId",
    "domainId",
    "hostId",
    "sourceId",
    "source",
    "priority",
    "insertDateTime",
    "retryCount",
  ],
};

const exampleData: IExampleData[] = [
  {
    jobId: "Job-1",
    jobType: "clean",
    topLevelId: 1,
    domainId: "1",
    hostId: "1",
    sourceId: "1",
    sources: ["fromIndexPage"],
    priority: 1,
    insertDateTime: "2023-06-01 11:15",
    scrapeStartDateTime: "2023-06-01 01:00",
    scrapeEndDateTime: "2023-06-01 01:10",
    scraperHost: "scraperHost-1",
    scraperContainer: "scraperContainer-1",
    errorMessage: "Failed",
    url: "www.job1.com",
    retryCount: 1,
    isError: false,
    taskType: "ttNonArticle",
  },
  {
    jobId: "Job-2",
    jobType: "scrape",
    topLevelId: 2,
    domainId: "2",
    hostId: "2",
    sourceId: "2",
    sources: ["manual", "fromIndexPage"],
    priority: 2,
    insertDateTime: "2023-06-05 12:20",
    scrapeStartDateTime: "2023-06-01 01:00",
    scrapeEndDateTime: "2023-06-01 01:10",
    scraperHost: "scraperHost-1",
    scraperContainer: "scraperContainer-1",
    errorMessage: "Failed",
    url: "www.asdasd.com",
    retryCount: 2,
    isError: true,
    taskType: "ttReportedProblem",
  },
  {
    jobId: "Job-3",
    jobType: "clean",
    topLevelId: 3,
    domainId: "3",
    hostId: "3",
    sourceId: "3",
    sources: ["fromIndexPage"],
    priority: 1,
    insertDateTime: "",
    scrapeStartDateTime: "2023-06-01 01:00",
    scrapeEndDateTime: "2023-06-01 01:10",
    scraperHost: "scraperHost-1",
    scraperContainer: "scraperContainer-1",
    errorMessage: "Failed",
    url: "www.job2.com",
    retryCount: 3,
    isError: false,
    taskType: "ttNoShaper",
  },
  {
    jobId: "Job-4",
    jobType: "clean",
    topLevelId: 3,
    domainId: "3",
    hostId: "3",
    sourceId: "3",
    sources: ["fromIndexPage"],
    priority: 1,
    insertDateTime: "2023-06-01 14:00",
    scrapeStartDateTime: "2023-06-01 01:00",
    scrapeEndDateTime: "2023-06-01 01:10",
    scraperHost: "scraperHost-1",
    scraperContainer: "scraperContainer-1",
    errorMessage: "Failed",
    url: "www.job2.com",
    retryCount: 3,
    isError: false,
    taskType: "ttVisualCheck",
  },
  {
    jobId: "Job-5",
    jobType: "clean",
    topLevelId: 3,
    domainId: "3",
    hostId: "3",
    sourceId: "3",
    sources: ["fromIndexPage"],
    priority: 1,
    insertDateTime: "2023-06-05 14:20",
    scrapeStartDateTime: "2023-06-01 01:00",
    scrapeEndDateTime: "2023-06-01 01:10",
    scraperHost: "scraperHost-1",
    scraperContainer: "scraperContainer-1",
    errorMessage: "Failed",
    url: "www.job2.com",
    retryCount: 3,
    isError: false,
    taskType: "ttNonArticle",
  },
];

export default exampleData;
